<?php
session_start();
include("header.php");
include "connection.php";
include "User.php";
?>
<?php
$con = new MyPdo();
$result = $con->getAllData("sport");
if(isset($_POST['sport_add_submit'])) {
    if(isset($_POST['new_sport'])) {
        $con->setDataToSport2(htmlspecialchars($_POST['new_sport']));
    }
}

if(isset($_POST['submit'])) {
    // check if user doesn't logged in and create new user
    if(!isset($_SESSION['email'])) {
        $newUser = new User();
        if(isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['depart']) &&
            isset($_POST['email']) && isset($_POST['sportList']) && isset($_POST['level'])) {
                $newUser->setDepartment(htmlspecialchars($_POST['depart']));
                $newUser->setFirstName(htmlspecialchars($_POST['firstName']));
                $newUser->setLastName(htmlspecialchars($_POST['lastName']));
                $newUser->setDepartment(htmlspecialchars($_POST['depart']));
                $newUser->setEmail(htmlspecialchars($_POST['email']));
                $newUser->setLevel(htmlspecialchars($_POST['level']));
                $newUser->setSportArray($_POST['sportList']);

                $lastId = $con->getLastId("person");
                if($lastId == 0)
                    $lastId = 0;
                //if user doesn't exist add new user
                $newUser->setId(++$lastId);
                //else if user exist update table pratique



                $con->setDataToPerson($newUser);
                foreach ($newUser->getSportArray() as $userSport) {
                    $prId = $con->getIdSportByName($userSport);
                    $con->setDataToPr($newUser->getId(), $prId, $newUser->getLevel());
                }

        } else {?>
<p><?php
            printf("%s", "ALL FIELDS MUST BE FILLED");?>
</p><?php
        }
    }
}
if(isset($_SESSION['email'])){
    $userFields = $con->getAll("person", $_SESSION['email']);
    $sportListOfConnectedUser = $con->getSportByPersonEmail($userFields[0]['email']);

    $names = $con->getSportsNotUsedByPerson($sportListOfConnectedUser);


    if(isset($_POST['submit'])) {
        if (isset($_POST['sportList']) && isset($_POST['level'])) {
            foreach ($_POST['sportList'] as $userSport) {
                $prId = $con->getIdSportByName($userSport);
                $con->setDataToPr($userFields[0]['id'], $prId, $_POST['level']);
            }
        }
    }
}

?>
<div class="reg_container_hor">
    <div>
        <form action="#" method="post">
            <input type="text" name="new_sport" placeholder="Enter a new Sport" />
            <input type="submit" value="Add" name="sport_add_submit">
        </form>
    </div>
</div>

<form method="post">
    <div class="reg_container_hor">
        <?php if(!isset($_SESSION['email'])) { ?>
            <div><p class="welcome_text">REGISTER PLEASE</p></div>
        <?php } else { ?>
            <div><p class="welcome_text"><?php printf('%s',$_SESSION['name']) ?>, add a new sport</p></div>
        <?php } ?>
        <div class="reg_container">
            <?php if(!isset($_SESSION['email'])) { ?>
            <div>
                <p class="small_header">Your BIO</p>
                <label for="first_name">Name</label><br/>
                <input type="text" name="firstName" id="first_name" />
                <br/><br/>
                <label for="last_name">Surname</label><br/>
                <input type="text" name="lastName" id="last_name" />
                <br/><br/>
                <label for="depart">Department</label><br/>
                <input type="text" name="depart" id="depart" />
                <br/><br/>
                <label for="email">Email</label><br/>
                <input type="email" name="email" id="email" />
            </div>
            <?php } else {?>
                <div>
                    <p class="small_header">Your sport's list</p>
                    <select multiple size=15 name="sportList[]" disabled>
                        <?php
                        foreach ($sportListOfConnectedUser as $item) {
                            ?>
                            <option ><?php printf("%s", $item); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            <?php }
            if(!isset($_SESSION['email'])) { ?>
            <div>
                <p class="small_header">Choose your Sport</p>
                <select multiple size=15 name="sportList[]">
                <?php
                foreach ($result as $item)  {
                    ?>
                    <option ><?php printf("%s", $item['name']); ?></option>
                    <?php
                }
                ?>
                </select>
            </div>
            <?php } else {?>
                <div>
                    <p class="small_header">Choose your Sport</p>
                    <select multiple size=15  name="sportList[]">
                        <?php
                        foreach ($names as $item) {
                            ?>
                            <option><?php printf("%s", $item); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            <?php } ?>
            <div>
                <p class="small_header">Choose your Level</p>
                <select size=15 name="level">
                    <option>Beginner</option>
                    <option>Intermediate</option>
                    <option>Advanced</option>
                    <option>Professional</option>
                </select>
            </div>
        </div>
        <br/><br/>
        <div><input type="submit" value="Registration" name="submit" /></div>
    </div>
</form>


<?php include("footer.php"); ?>
