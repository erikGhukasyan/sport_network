<?php
class MyPdo {
    private $myPdo;

    public function __construct() {
        try {
            $this->myPdo = new PDO('pgsql:host=localhost;dbname=sportdb', 'Erik', '');
            $this->myPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
        }
    }

    /**
     * @return mixed
     */
    public function getMyPdo() {
        return $this->myPdo;
    }
    public function getNameSportById($id) {
        $data = $this->myPdo->prepare("SELECT name FROM sport WHERE id = '$id'");
        $data->execute();
        $result = $data ->fetchAll();
        return $result;
    }
    public function getIdSportByName($name) {
        $data = $this->myPdo->prepare("SELECT id FROM sport WHERE name = '$name'");
        $data->execute();
        $result = $data ->fetch(PDO::FETCH_ASSOC);
        return $result['id'];
    }
    public function getLastId($table) {
        $data = $this->myPdo->prepare("SELECT $table.id FROM ".$table." ORDER BY $table.id DESC  LIMIT 1");
        $data->execute();
        $f = $data ->fetch();
        $result = $f['id'];
        return $result;
    }
    public function getAllData($table) {
        $data = $this->myPdo->prepare("SELECT * FROM ".$table);
        $data->execute();
        $result = $data ->fetchAll();
        return $result;
    }

    public function getAll($table, $email) {
        $data = $this->myPdo->prepare("SELECT * FROM ".$table." WHERE $table.email = '".$email."'");
        $data->execute();
        $result = $data ->fetchAll();
        return $result;
    }

// INSERT INTO films VALUES ('UA502', 'Bananas', 105, '1971-07-13', 'Comédie', '82 minutes');
    public function setDataToPerson(User $person) {
        $query = $this->myPdo->prepare("INSERT INTO person 
        VALUES ('".$person->getId()."', '".$person->getFirstName()."', '".$person->getLastName()."', '".$person->getEmail()."', '".$person->getDepartment()."')");
        $query->execute();
    }



    public function setDataToSport2($data) {
        $query = $this->myPdo->prepare("INSERT INTO sport (name) VALUES ('$data')");
        $query->execute();
    }

    public function updateData($table, $data) {

    }

    public function setDataToPr($idUser, $idSport, $level) {
        $query = $this->myPdo->prepare("INSERT INTO pratique VALUES ('$idUser', $idSport, '$level')");
        $query->execute();
    }

    // SELECT u.id FROM users u JOIN user_group ug on ug.user_id = u.id
    // group by u.id
    //having array_agg(ug.group_id order by ug.group_id) = array[1,2,3];

    public function getSportByPersonEmail($email) {
        $personId = $this->getIdPersonByEmail($email);
        $data = $this->myPdo->prepare("SELECT s.name FROM sport s JOIN pratique pr on pr.person_id='$personId' WHERE s.id=pr.sport_id");
        $data->execute();
        $result = $data ->fetchAll(PDO::FETCH_COLUMN, 0);
        return $result;
    }

    //  SELECT u.id
    //  FROM users u
    //  JOIN user_group ug on ug.user_id = u.id
    //  group by u.id

    public function getSportsNotUsedByPerson($e) {
        $names = join("','",$e);
        $data = $this->myPdo->prepare("SELECT name FROM sport WHERE name NOT IN ('".$names."') ");
        $data->execute();
        $result = $data ->fetchAll(PDO::FETCH_COLUMN, 0);
        return $result;
    }

    public function getIdPersonByEmail($email) {
        $data = $this->myPdo->prepare("SELECT id FROM person WHERE email='$email'");
        $data->execute();
        $result = $data ->fetch(PDO::FETCH_ASSOC);
        return $result['id'];
    }


    public function getPersonsBySports($e) {
        $names = join("','",$e);
        $data = $this->myPdo->prepare("SELECT * FROM person INNER JOIN pratique ON pratique.person_id=person.id INNER JOIN sport ON pratique.sport_id = sport.id  WHERE sport.name IN ('".$names."')");
        $data->execute();
        $result = $data ->fetchAll();
        return $result;
    }




}

