<?php session_start();
include "connection.php";
$con = new MyPdo();
if (isset($_GET["e"])) {
    session_destroy();
    unset($_GET["e"]);
    unset($_SESSION['email']);
    unset($_SESSION['name']);
    unset($_SESSION['lastName']);
}

if(isset($_POST['email'])) {
    $email = htmlspecialchars($_POST['email']);
    unset($_POST['email']);
    $result = $con->getAll("person", $email);
    /*echo '<pre>';
    var_dump($result);
    echo '</pre>';
    echo $result[0]['first_name']." ".$result[0]['last_name'];*/
    if(sizeof($result) !== 0) {
        $_SESSION['email'] = $email;
        $_SESSION['name'] = $result[0]['first_name'];
        $_SESSION['lastName'] = $result[0]['last_name'];
    } else {
        unset($_SESSION['email']);
        header('Location: ajout.php');
    }
}
if(isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
    $name = $_SESSION['name'];
    $lastName = $_SESSION['lastName'];

}

?>
<?php include("header.php"); ?>
<?php
if(!isset( $_SESSION['email'] ) ) {
?>
    <div class="login_form">
        <form action="index.php" method="post">
            <input type="text" name="email" class="email_field" placeholder="E-mail" />
            <input type="submit" value="Login" />
        </form>
    </div>
    <?php
} else {
?>
    <div class="reg_p_menu">
        <p class="welcome_text">WELCOME <?php printf('%s', $name." ".$lastName) ?></p>
        <div>
            <a class="index_links" href="recherche.php">Research</a>
            <a class="index_links" href="ajout.php">Add</a>
        </div>
    </div>

    <?php
    }
?>
<?php include("footer.php"); ?>





